package br.com.lymnet.apimonitor;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/monitor")
public class MonitorController {

	@GetMapping("/info")
	  public String info() {
		  String data = "";
		  try {
			data = InetAddress.getLocalHost().getHostAddress() +  "/" + InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			data = "Erro";
		}
	    return "Info - teste " + data ;
	  }
	
	@GetMapping("/teste") 
	public String teste() {
		return "Teste OK";
	}
}
